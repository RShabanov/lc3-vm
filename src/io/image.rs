use std::{
    fs::File,
    io::{self, BufReader, Read},
};

use byteorder::{BigEndian, ReadBytesExt};

use crate::hardware::memory::Memory;

pub fn read_image(image_path: String) -> io::Result<Memory> {
    let file = File::open(image_path)?;
    let buf = BufReader::new(file);

    load_into_memory(buf)
}

fn load_into_memory<R>(mut rdr: R) -> io::Result<Memory>
where
    R: Read,
{
    let origin = rdr.read_u16::<BigEndian>()?;
    let mut address = origin as usize;
    let mut mem = Memory::new();

    loop {
        match rdr.read_u16::<BigEndian>() {
            Ok(instruction) => {
                mem.write(address as u16, instruction);
                address += 1;
            }
            Err(e) => {
                return if e.kind() == io::ErrorKind::UnexpectedEof {
                    Ok(mem)
                } else {
                    Err(e)
                }
            }
        }
    }
}
