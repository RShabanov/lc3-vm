pub const REGISTER_COUNT: usize = 10;

pub enum RegisterName {
    R0,
    R1,
    R2,
    R3,
    R4,
    R5,
    R6,
    R7,
    Pc,   // program counter
    Cond, // condition flags
}

impl Into<u16> for RegisterName {
    fn into(self) -> u16 {
        match self {
            RegisterName::R0 => 0,
            RegisterName::R1 => 1,
            RegisterName::R2 => 2,
            RegisterName::R3 => 3,
            RegisterName::R4 => 4,
            RegisterName::R5 => 5,
            RegisterName::R6 => 6,
            RegisterName::R7 => 7,
            RegisterName::Pc => 8,
            RegisterName::Cond => 9,
        }
    }
}
