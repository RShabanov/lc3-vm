use self::name::{RegisterName, REGISTER_COUNT};

pub mod condition_flags;
pub mod name;

pub struct Registers {
    reg: [u16; REGISTER_COUNT],
}

impl Registers {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn set(&mut self, index: u16, value: u16) {
        match index {
            0..=15 => self.reg[index as usize] = value,
            _ => panic!("Invalid register (index out of range): {index}"),
        }
    }

    pub fn set_by_name(&mut self, name: RegisterName, value: u16) {
        self.set(name.into(), value)
    }

    pub fn get(&self, index: u16) -> u16 {
        match index {
            0..=15 => self.reg[index as usize],
            _ => panic!("Invalid register (index out of range): {index}"),
        }
    }

    pub fn get_by_name(&self, name: RegisterName) -> u16 {
        self.get(name.into())
    }

    pub fn offset_pc(&mut self, offset: u16) {
        let pc = self
            .get_by_name(RegisterName::Pc)
            .wrapping_add(offset);
        self.set_by_name(RegisterName::Pc, pc);
    }

    pub fn update_flags(&mut self, index: u16) {
        let reg_value = self.get(index);

        self.set_by_name(
            RegisterName::Cond,
            if reg_value == 0 {
                condition_flags::ZRO
            } else if (reg_value >> 15) != 0 {
                condition_flags::NEG
            } else {
                condition_flags::POS
            },
        );
    }
}

impl Default for Registers {
    fn default() -> Self {
        Self {
            reg: [0; REGISTER_COUNT],
        }
    }
}
