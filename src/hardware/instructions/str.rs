use crate::hardware::{memory::Memory, registers::Registers};

use super::sign_extend;

pub fn str(instr: u16, registers: &Registers, memory: &mut Memory) {
    let sr = (instr >> 9) & 0x7;
    let base_r = (instr >> 6) & 0x7;
    let offset = sign_extend(instr & 0x3f, 6);

    let address = registers
        .get(base_r)
        .wrapping_add(offset);

    memory.write(address, registers.get(sr));
}
