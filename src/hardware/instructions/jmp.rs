use crate::hardware::registers::{name::RegisterName, Registers};

pub fn jmp(instr: u16, registers: &mut Registers) {
    let base_r = (instr >> 6) & 0x7;

    let address = registers.get(base_r);
    registers.set_by_name(RegisterName::Pc, address);
}
