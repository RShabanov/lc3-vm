use crate::hardware::registers::{name::RegisterName, Registers};

use super::sign_extend;

pub fn lea(instr: u16, registers: &mut Registers) {
    let dr = (instr >> 9) & 0x7;
    let pc_offset = sign_extend(instr & 0x1ff, 9);

    let address = registers
        .get_by_name(RegisterName::Pc)
        .wrapping_add(pc_offset);

    registers.set(dr, address);
    registers.update_flags(dr);
}
