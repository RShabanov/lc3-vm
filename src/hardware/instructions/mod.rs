use self::{
    add::add, and::and, br::br, jmp::jmp, jsr::jsr, ld::ld, ldi::ldi, ldr::ldr, lea::lea, not::not,
    st::st, sti::sti, str::str, trap::trap,
};

use super::{memory::Memory, registers::Registers};

mod add;
mod and;
mod br;
mod jmp;
mod jsr;
mod ld;
mod ldi;
mod ldr;
mod lea;
mod not;
mod st;
mod sti;
mod str;
mod trap;

#[derive(PartialEq, Debug)]
pub enum Instruction {
    Br,   // branch
    Add,  // add
    Ld,   // load
    St,   // store
    Jsr,  // jump register
    And,  // bitwise and
    Ldr,  // load register
    Str,  // store register
    Rti,  // unused
    Not,  // bitwise not
    Ldi,  // load indirect
    Sti,  // store indirect
    Jmp,  // jump
    Res,  // reserved (unused)
    Lea,  // load effective address
    Trap, // execute trap
}

impl Instruction {
    fn get(code: u16) -> Option<Instruction> {
        match code >> 12 {
            0 => Some(Instruction::Br),
            1 => Some(Instruction::Add),
            2 => Some(Instruction::Ld),
            3 => Some(Instruction::St),
            4 => Some(Instruction::Jsr),
            5 => Some(Instruction::And),
            6 => Some(Instruction::Ldr),
            7 => Some(Instruction::Str),
            8 => Some(Instruction::Rti),
            9 => Some(Instruction::Not),
            10 => Some(Instruction::Ldi),
            11 => Some(Instruction::Sti),
            12 => Some(Instruction::Jmp),
            13 => Some(Instruction::Res),
            14 => Some(Instruction::Lea),
            15 => Some(Instruction::Trap),
            _ => None,
        }
    }

    pub fn execute(instr: u16, registers: &mut Registers, memory: &mut Memory) {
        if let Some(instruction) = Self::get(instr) {
            match instruction {
                Instruction::Br => br(instr, registers),
                Instruction::Add => add(instr, registers),
                Instruction::Ld => ld(instr, registers, memory),
                Instruction::St => st(instr, registers, memory),
                Instruction::Jsr => jsr(instr, registers),
                Instruction::And => and(instr, registers),
                Instruction::Ldr => ldr(instr, registers, memory),
                Instruction::Str => str(instr, registers, memory),
                Instruction::Not => not(instr, registers),
                Instruction::Ldi => ldi(instr, registers, memory),
                Instruction::Sti => sti(instr, registers, memory),
                Instruction::Jmp => jmp(instr, registers),
                Instruction::Lea => lea(instr, registers),
                Instruction::Trap => trap(instr, registers, memory),
                _ => {}
            }
        }
    }
}

// converts a x bit number into an equivalent 16 bit number
fn sign_extend(mut n: u16, bit_count: u8) -> u16 {
    if (n >> (bit_count - 1)) & 1 != 0 {
        n |= 0xffff << bit_count;
    }
    n
}

#[cfg(test)]
mod instruction_test {
    use super::*;

    #[test]
    fn get_instruction() {
        let instrs = [
            Some(Instruction::Br),
            Some(Instruction::Add),
            Some(Instruction::Ld),
            Some(Instruction::St),
            Some(Instruction::Jsr),
            Some(Instruction::And),
            Some(Instruction::Ldr),
            Some(Instruction::Str),
            Some(Instruction::Rti),
            Some(Instruction::Not),
            Some(Instruction::Ldi),
            Some(Instruction::Sti),
            Some(Instruction::Jmp),
            Some(Instruction::Res),
            Some(Instruction::Lea),
            Some(Instruction::Trap),
        ];

        for code in 0..16u16 {
            assert_eq!(Instruction::get(code << 12), instrs[code as usize]);
        }
    }
}

#[cfg(test)]
mod sign_extend_test {
    use super::sign_extend;

    #[test]
    fn simple_test() {
        let target = 0b1111111111111101;
        let mut n = 0b101;
        n = sign_extend(n, 3);

        assert_eq!(n, target);
    }
}
