use crate::hardware::{memory::Memory, registers::Registers};

use super::sign_extend;

pub fn ldr(instr: u16, registers: &mut Registers, memory: &mut Memory) {
    let dr = (instr >> 9) & 0x7;
    let base_r = (instr >> 6) & 0x7;
    let offset = sign_extend(instr & 0x3f, 6);
    let address = registers
        .get(base_r)
        .wrapping_add(offset);

    registers.set(dr, memory.read(address));
    registers.update_flags(dr);
}
