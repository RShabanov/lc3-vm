use crate::hardware::registers::Registers;

pub fn not(instr: u16, registers: &mut Registers) {
    let dr = (instr >> 9) & 0x7;
    let sr = (instr >> 6) & 0x7;

    registers.set(dr, !registers.get(sr));
    registers.update_flags(dr);
}
