use crate::hardware::registers::{name::RegisterName, Registers};

use super::sign_extend;

pub fn br(instr: u16, registers: &mut Registers) {
    let pc_offset = sign_extend(instr & 0x1ff, 9);
    let cond_flags = (instr >> 9) & 0x7;

    if cond_flags & registers.get_by_name(RegisterName::Cond) != 0 {
        registers.offset_pc(pc_offset);
    }
}
