use crate::hardware::{
    memory::Memory,
    registers::{name::RegisterName, Registers},
};

use super::sign_extend;

pub fn sti(instr: u16, registers: &Registers, memory: &mut Memory) {
    let sr = (instr >> 9) & 0x7;
    let mut address = registers
        .get_by_name(RegisterName::Pc)
        .wrapping_add(sign_extend(instr & 0x1ff, 9));
    address = memory.read(address);

    memory.write(address, registers.get(sr));
}
