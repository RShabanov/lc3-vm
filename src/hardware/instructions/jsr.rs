use crate::hardware::registers::{name::RegisterName, Registers};

use super::sign_extend;

pub fn jsr(instr: u16, registers: &mut Registers) {
    let pc = registers.get_by_name(RegisterName::Pc);
    registers.set_by_name(RegisterName::R7, pc);

    if (instr >> 11) & 1 == 0 {
        let base_r = (instr >> 6) & 0x7; // JSRR
        registers.set_by_name(RegisterName::Pc, registers.get(base_r));
    } else {
        registers.offset_pc(sign_extend(instr & 0x7ff, 11)); // JSR
    }
}
