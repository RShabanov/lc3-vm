use std::{
    io::{self, Write},
    process,
};

use crate::{
    hardware::{
        memory::Memory,
        registers::{name::RegisterName, Registers},
    },
    io::get_char,
    sys::terminal,
};

pub enum TrapCode {
    Getc,  // get character from keyboard, not echoed onto the terminal
    Out,   // output a character
    Puts,  // output a word string
    In,    // get character from keyboard, echoed onto the terminal
    Putsp, // output a byte string
    Halt,  // halt the program
}

pub fn trap(instr: u16, registers: &mut Registers, memory: &mut Memory) {
    let pc = registers.get_by_name(RegisterName::Pc);
    registers.set_by_name(RegisterName::R7, pc);

    terminal::turn_off_canonical_and_echo_modes();

    match (instr & 0xff).into() {
        TrapCode::Getc => getc(registers),
        TrapCode::Out => out(registers),
        TrapCode::Puts => puts(registers, memory),
        TrapCode::In => in_fn(registers),
        TrapCode::Putsp => putsp(registers, memory),
        TrapCode::Halt => halt(),
    }

    terminal::restore_terminal_settings();
}

impl From<u16> for TrapCode {
    fn from(value: u16) -> Self {
        match value {
            0x20 => Self::Getc,
            0x21 => Self::Out,
            0x22 => Self::Puts,
            0x23 => Self::In,
            0x24 => Self::Putsp,
            0x25 => Self::Halt,
            _ => {
                println!("Invalid value for trap code.");
                terminal::restore_terminal_settings();
                process::exit(1);
            }
        }
    }
}

#[inline]
fn puts(registers: &Registers, memory: &Memory) {
    let index = registers.get_by_name(RegisterName::R0) as usize;

    memory.cells[index..]
        .iter()
        .take_while(|x| **x != 0)
        .for_each(|x| print!("{}", (*x as u8) as char));

    io::stdout().flush().expect("Flushed");
}

#[inline]
fn putsp(registers: &Registers, memory: &Memory) {
    let index = registers.get_by_name(RegisterName::R0) as usize;

    memory.cells[index..]
        .iter()
        .take_while(|x| **x != 0)
        .flat_map(|x| x.to_be_bytes())
        .for_each(|x| print!("{}", x as char));

    io::stdout().flush().expect("Flushed");
}

#[inline]
fn getc(registers: &mut Registers) {
    registers.set_by_name(RegisterName::R0, get_char() as u16);
}

#[inline]
fn out(registers: &Registers) {
    let ch = (registers.get_by_name(RegisterName::R0) as u8) as char;
    print!("{ch}");

    io::stdout().flush().expect("Flushed");
}

#[inline]
fn in_fn(registers: &mut Registers) {
    print!("Enter a character: ");
    io::stdout().flush().expect("Flushed");
    registers.set_by_name(RegisterName::R0, get_char() as u16);
}

#[inline]
fn halt() {
    print!("Halt");
    io::stdout().flush().expect("Flushed");

    terminal::restore_terminal_settings();
    process::exit(1);
}
