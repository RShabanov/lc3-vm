use crate::hardware::registers::Registers;

use super::sign_extend;

pub fn add(instr: u16, registers: &mut Registers) {
    let dr = (instr >> 9) & 0x7;
    let sr1 = (instr >> 6) & 0x7;
    let imm_flag = (instr >> 5) & 0x1;

    let value = if imm_flag == 1 {
        let imm5 = sign_extend(instr & 0x1f, 5);
        registers.get(sr1).wrapping_add(imm5)
    } else {
        let sr2 = instr & 0x7;
        registers
            .get(sr1)
            .wrapping_add(registers.get(sr2))
    };

    registers.set(dr, value);
    registers.update_flags(dr);
}
