use crate::hardware::{
    memory::Memory,
    registers::{name::RegisterName, Registers},
};

use super::sign_extend;

pub fn ldi(instr: u16, registers: &mut Registers, memory: &mut Memory) {
    let dr = (instr >> 9) & 0x7;
    let pc_offset = sign_extend(instr & 0x1ff, 9);

    let mut address = registers
        .get_by_name(RegisterName::Pc)
        .wrapping_add(pc_offset);
    address = memory.read(address);

    registers.set(dr, memory.read(address));
    registers.update_flags(dr);
}
