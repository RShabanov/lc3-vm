use self::{
    instructions::Instruction,
    memory::Memory,
    registers::{name::RegisterName, Registers},
};

pub mod instructions;
pub mod memory;
pub mod registers;

pub fn execute_program(mut mem: Memory) {
    let mut registers = Registers::new();

    loop {
        // read instruction
        let instruction = mem.read(registers.get_by_name(RegisterName::Pc));

        // increment program counter
        registers.offset_pc(1);

        // execute
        Instruction::execute(instruction, &mut registers, &mut mem);
    }
}
