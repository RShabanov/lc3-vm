use libc::STDIN_FILENO;

use crate::{
    io::get_char,
    sys::{
        select::{self, FdSet},
        time::{TimeVal, TimeValLike},
    },
};

mod mapped_registers;

pub const MEMORY_SIZE: usize = std::u16::MAX as usize + 1;

pub struct Memory {
    pub cells: [u16; MEMORY_SIZE],
}

impl Memory {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn read(&mut self, address: u16) -> u16 {
        use mapped_registers::*;
        if address == KBSR as u16 {
            if check_key() {
                self.cells[KBSR] = 1 << 15;
                self.cells[KBDR] = get_char() as u16;
            } else {
                self.cells[KBSR] = 0;
            }
        }

        self.cells[address as usize]
    }

    pub fn write(&mut self, address: u16, value: u16) {
        self.cells[address as usize] = value;
    }
}

impl Default for Memory {
    fn default() -> Self {
        Self {
            cells: [0; MEMORY_SIZE],
        }
    }
}

fn check_key() -> bool {
    let mut fd = FdSet::new();
    fd.insert(STDIN_FILENO);

    let mut timeout = TimeVal::seconds(0);

    !matches!(select::select(1, &mut fd, None, None, &mut timeout), Err(_))
}

#[cfg(test)]
mod memory_test {
    use super::*;

    const EXPECTED_MEMORY_SIZE: usize = 65536;

    #[test]
    fn memory_size() {
        let memory = Memory::new();
        assert_eq!(memory.cells.len(), EXPECTED_MEMORY_SIZE);
    }

    #[test]
    fn memory_size_constant() {
        assert_eq!(MEMORY_SIZE, EXPECTED_MEMORY_SIZE);
    }
}
