use std::env::Args;

use hardware::memory::Memory;

pub mod hardware;
pub mod io;
pub mod sys;

pub fn handle_args(mut args: Args) -> Result<Memory, &'static str> {
    args.next();
    match args.next() {
        Some(arg) => match io::image::read_image(arg) {
            Ok(mem) => Ok(mem),
            Err(_) => Err("Error encountered while reading into memory."),
        },
        None => Err("No more file for processing."),
    }
}
