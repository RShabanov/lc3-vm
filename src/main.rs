use std::{env, error::Error, process};

use lc3_vm::{handle_args, hardware, sys::terminal};

pub const PC_START: u16 = 0x3000;

fn main() -> Result<(), Box<dyn Error>> {
    terminal::spawn_control_c_handler()?;

    match handle_args(env::args()) {
        Ok(mem) => {
            // execute program
            hardware::execute_program(mem);
            // restore terminal settings
            terminal::restore_terminal_settings();
            // return
            Ok(())
        }
        Err(e) => {
            println!("{e}");
            // restore terminal settings
            terminal::restore_terminal_settings();
            // exit
            process::exit(1);
        }
    }
}
